#include <RH_ASK.h>
#include <SPI.h> // Not actually used but needed to compile

RH_ASK driver;
const int buttonPin1 = 2;     //Blink Right
const int buttonPin2 =  3;    //Blink Left
const int buttonPin3 =  4;   //Turn off
 // variables will change:
  int buttonState1 = 0;     
  int buttonState2 = 0; 
  int buttonState3 = 0; 
void setup()
{
  // initialize the LED pin as an output:
  pinMode(buttonPin2, INPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin1, INPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin3, INPUT);

    Serial.begin(9600);	  // Debugging only
    if (!driver.init())
         Serial.println("init failed");
}

void loop()
{
// read the state of the pushbutton value:
  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  buttonState3 = digitalRead(buttonPin3);
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState1 == HIGH) {
    
    const char *msg = "#1";
    driver.send((uint8_t *)msg, strlen(msg));
    driver.waitPacketSent();
    
  }
  else{
     if (buttonState2 == HIGH) {
    
    const char *msg = "#2";
    driver.send((uint8_t *)msg, strlen(msg));
    driver.waitPacketSent();
      
    }else{
          if (buttonState3 == HIGH) {
            const char *msg = "#0";
            driver.send((uint8_t *)msg, strlen(msg));
            driver.waitPacketSent();
    }else{
      delay(1);
    }
    }
  }
 
   
  
    
   
    
}
