<h1 align="center" style="display: block; font-size: 2.5em; font-weight: bold; margin-block-start: 1em; margin-block-end: 1em;">
  <br><br><strong>Intelligent Bicycle Light</strong>
</h1>

# Table of contents

1. [Basics about the project](#1-basics-about-the-project) <br>
    1.1 [Intended Use](#11-intended-use) <br>
    1.2 [Project State](#12-project-state) <br>
2. [Overview](#2-overview) <br>
    2.1 [Project Structure](#21-project-structure) <br>
    2.2 [Requirements](#22-requirements) <br>
        2.2.1 [Lights](#221-lights)<br>
        2.2.2 [Sensors](#222-sensors)<br>
        2.2.3 [Controlling](#223-controlling)<br>
        2.2.4 [Housings](#224-housings)<br>
        2.2.5 [Energy](#225-energy)<br>
    2.3 [What You Need](#23-what-you-need) <br>
        2.3.1 [Software for Development and Testing](#231-software-for-development-and-testing)<br>
        2.3.2 [Hardware for Development](#232-hardware-for-development)<br>
        2.3.3 [Hardware for Testing](#233-hardware-for-testing)<br>
3. [Connections & Pinout](#3-connections-&-pinout) <br>
    3.1 [Backlight](#31-Backlight) <br>
    3.2 [Indicator Controller](#32-indicator-controller) <br>
4. [The application in depth](#4-the-application-in-depth) <br>
    4.1 [Backlight](#41-Backlight) <br>
        4.1.1 [GPS Module](#411-gps-module) <br>
        4.1.2 [Accelerometer](#412-accelerometer) <br>
        4.1.3 [433Mhz Receiver](#413-433mhz-receiver) <br>
        4.1.4 [LED Control](#414-led-control) <br>
    4.2 [Indicator Controller](#42-indicator-controller) <br>


# 1. Basics about the project

## 1.1 Intended Use
The bike light is intended to improve road safety. The rear light flashes faster or slower depending on the speed. When braking, the rear light is switched on at maximum brightness for the duration of that process. With a wireless controller on the steering wheel you can indicate a turning maneuver.
  
![Backlight Example](./docs/vids/backlight_example.mp4)

![Indicator Example](./docs/vids/indicator_example.mp4)

## 1.2 Project State
- still in development
- working prototype but no finished application
- Still Missing:
  - Housing and Hardware Design
  - Battery Connection with Charching Module
  - RF-Receiver not working very good
  - Indicator Controller without Arduino!?
    - better use a encoder and decoder IC


# 2. Overview

## 2.1 Project Structure
![](./docs/img/overview2.png)

## 2.2 Requirements

## 2.2.1 Lights
- The rear light should flash faster or slower depending on the speed of the bike. The faster the driver moves the bike, the faster the flashing frequency of the light should be.
- In the event of braking, the rear light should come on at maximum brightness.
- When turning, the left or right indicator should be turned on accordingly.
## 2.2.2 Sensors
- A GPS module should measure the current speed of the bicycle
- A accelerometer should detect a braking process through negative acceleration
- A 433 MHz transmitter and receiver should establish communication between the indicator contoller and taillight.
## 2.2.3 Controlling
- The control should be taken over by a microcontroller, which regulates the light depending on all controllers, switches and sensors.
- An on/off switch to switch the bike light on and off
- With the help of a controller on the steering wheel, it should be possible to indicate a turning maneuver
## 2.2.4 Housings
- The housing for the rear light should be as stable and impact-resistant as possible so that it is not destroyed in an accident.
- It should be waterproof so that the bike light can also be used at bad weather conditions
- It should be universally mountable on all kinds of bicycles
- The same applies to the indicator housing, but it must be mounted on the handlebars
## 2.2.5 Energy
- The wheel light should be operated with a rechargeable battery
- In order to achieve the longest possible operating time between charging cycles, all components should consume as little energy as possible

## 2.3 What You Need

### 2.3.1 Software for Development and Testing
- [Arduino IDE](https://www.arduino.cc/en/software)
### 2.3.2 Hardware for Development
- [Arduino Nano](https://store.arduino.cc/collections/boards/products/arduino-nano)
- GPS Module: [Seamuing GPS Modul](https://www.amazon.de/gp/product/B0833TMYQ3/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)
- Accelerometer: [LIS3DH](https://www.digikey.com/catalog/en/partgroup/lis3dh-triple-axis-accelerometer-2g-4g-8g-16g/57890)
- [433Mhz Transmitter and Receiver](https://www.amazon.de/gp/product/B00OLI93IC/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1)
- LI-PO Batterys: [3,7V 220mAh LiPo Akku 35C](https://www.amazon.de/gp/product/B07D3864QN/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1)
- Several LED's
- Push Buttons
### 2.3.3 Hardware for Testing
- Breadboards
- Wires
- USB Cabel for the Arduino Nano

---
---


## 3. Connections & Pinout
## 3.1 Backlight
|  Arduino      |   GPS Module  |
| :-----------: |:-------------:|
| D4            | RX            |
| D3            | TX            |
| +5V           | VCC           |
| GND           | GND           |

|  Arduino      |  Accelerometer|
| :-----------: |:-------------:|
| A5            | SCL           |
| A4            | SDL           |
| +5V           | VCC           |
| GND           | GND           |

|  Arduino      |433MHz Receiver|
| :-----------: |:-------------:|
| D11           | DATA          |
| +5V           | VCC           |
| GND           | GND           |

|  Arduino      |   LED'S         |
| :-----------: |:-------------:  |
| D12           | Backlights      |
| D8            | Right Indicator |
| D7            | Left Indicator  |
| GND           | GND             |

![](./docs/img/Backlight.png)

## 3.2 Incicator Controller
|  Arduino      |433MHz Receiver|
| :-----------: |:-------------:|
| D12           | DATA          |
| +5V           | VCC           |
| GND           | GND           |

|  Arduino      |   Buttons       |
| :-----------: |:-------------:  |
| D2            | Indicate Right  |
| D3            | Indicate Left   |
| D4            | Stop            |
| GND           | GND             |

![](./docs/img/Indicator_Controller.png)

# 4. The application in depth
All programs were written in the Arduino IDE with the typical structure `setup(){}` and `loop(){}`.
## 4.1 Backlight
The taillight consists of the GPS sensor, the accelerometer, the 433Mhz (or RF - Radio Frequency) receiver and the brain of the device, an Arduino Nano. Those components including the red rear/brake LEDs and the two groups of amber LEDs are housed in one housing.

**NOTE** 💡 The Housing is still in Development

## 4.1.1 GPS Module
The GPS module is responsible for measuring the speed. The TinyGPS++ library was used for implementation, which takes over the communication between the microcontroller and the module. Actually this library is only compatible with the GPS chip MEO-6M but after many attempts, it also works with the slightly more accurate GT-U7 used in this project.
This library is included with `#include <TinyGPS++.h>`.


```cpp
//TX and RX for the GPS
static const int TXPin = 4, RXPin = 3;
static const uint32_t GPSBaud = 9600;
// The TinyGPS++ object
TinyGPSPlus gps;
// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);
```
> Communication takes place via UART with a baud rate of 9600 symbols/second specified by the manufacturer.


```cpp
 //start communication with GPS module
  ss.begin(GPSBaud);
```
> In the `setup(){}` block the serial communication is started

```cpp
 // check for GPS data
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      getSpeed();

  if (millis() > 5000 && gps.charsProcessed() < 10) {
    Serial.println(F("No GPS detected: check wiring."));
    while (true)
      ;
  }
}
```
> In the `loop(){}` block it is queried whether data from the GPS module is available. If so, the data is read, encoded and the `getSpeed()` method is called.

```cpp
//process the speed in km/h
void getSpeed() {
  if (gps.speed.isValid()) {
    Serial.print(F("KMH:  "));
    Serial.print(gps.speed.kmph());
    interval = intervalRef - (gps.speed.kmph() * 20);
    if (interval < 200) {
      interval = 200;
    }
  } else {
    interval = intervalRef;
    Serial.print(F("INVALID"));
  }
  Serial.println();
}
```
> The `interval` is set depending on the measured speed. The km/h are multiplied by a fixed factor and then subtracted from the reference value. The lower limit for the interval was set to 200 milliseconds.

## 4.1.2 Accelerometer
A braking process is to be detected with the accelerometer. Therefor the Adafruit LIS3DH was used. For the implementation, the library provided by the manufacturer was used, which is included with `#include <Adafruit_LIS3DH.h>`.

```cpp
 // I2C
Adafruit_LIS3DH lis = Adafruit_LIS3DH();
```
> The communication between arduino and the sensor is done with the I2C bus protocol, which is implemented by default in the library. It would also be possible to use the SPI protocoll.

```cpp
 //start te Accelerometer
  Serial.println("LIS3DH test!");

  if (!lis.begin(0x18)) {  // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("LIS3DH found!");

  lis.setRange(LIS3DH_RANGE_2_G);  // 2, 4, 8 or 16 G!

  Serial.print("Range = ");
  Serial.print(2 << lis.getRange());
  Serial.println("G");
```
> In the `setup(){}` block, the sensor is initialized with the address for the I2C bus taken from the data sheet and the range is set to 2G. The raw value of this sensor ranges from -32768 to +32768 (16 bit range) and is scaled from +/-2G to +/-16G depending on the set range. Since this application is a bicycle light, +/-2G is more than sufficient.

```cpp
//get the acceleration data
  while (1) {
    lis.read();
    sensors_event_t event;
    lis.getEvent(&event);

    if (event.acceleration.x < -0.7) {
      digitalWrite(ledPin, HIGH);
      delay(100);
      continue;
    } else {
      break;
    }
  }
```
>The value of the sensor is queried and converted from the raw value into the acceleration value "meters per second squared". The program remains in this loop and sets the rear light to HIGH as long as the measured value is below -0.7G.

**NOTE** 💡 The perfect threshhold for the negativ accelaration is still in testing...

## 4.1.3 433Mhz Receiver
The RF receiver is a simple module sold by many unnamed manufacturers. In order to be able to interpret the received values, the RadioHead library is used, which works with all common RF transmitters/receivers.
The `#include <RH_ASK.h>` file is included because the transmitter and receiver convert the data using amplitude shift keying.

```cpp
//driver for the RF Receiver
RH_ASK driver;
.
.
.
//init 433Mhz driver
  if (!driver.init())
    Serial.println("init failed");
```
> First an object of this included class is created and initialized in the `setup(){}` block.

```cpp
//get messages from the 433Mhz Transmitter
  if (driver.recv(buf, &buflen))  // Non-blocking
  {
    // Message with a good checksum received, dump it.
    Serial.print("Message: ");
    Serial.println(buf[1]);
  }
```
>If a message is available, it is written into the variable `buf`. You can see how this variable is processed in the next paragraph [LED Controll](414-led-controll).

**NOTE** 💡 to not use the library an encode and decode IC will be used. Still in testing...

## 4.1.4 LED Control
The idea is to turn the LEDs on and off without using the `delay()` function. That's why a timer of the arduino was used so that the program is not blocked in the runtime.

```cpp
void tickerLED() {
  // check to see if it's time to blink the LED; that is, if the difference
  // between the current time and last time you blinked the LED is bigger than
  // the interval at which you want to blink the LED.
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);

    //set the indicator Lights in the same tik as the backlight
    if (buf[1] == 49) {
      digitalWrite(indicatorRight, ledState);
    } else {
      digitalWrite(indicatorRight, LOW);
    }
    if (buf[1] == 50) {
      digitalWrite(indicatorLeft, ledState);
    } else {
      digitalWrite(indicatorLeft, LOW);
    }
    if (buf[1] == 48) {
      digitalWrite(indicatorLeft, LOW);
      digitalWrite(indicatorRight, LOW);
    }
  }
}
```
> In the function, the current time is queried and compared with the last time depending on the `interval` variable and thus the clock frequency of the LEDs is realized. The `buf` variable is also processed here and, depending on the message received, the right or left indicator is switched on or the indicators are switched off.


## 4.2 Indicator Controller
The Indicator Controller consists of the RF transmitter and also an Arduino Nano. In addition, three buttons are used to turn left and right or turn off the turn signal. The same RadioHead library as before was used for the implementation.

**NOTE** 💡 like in the paragraph above, if an encoder is used, there would be no need for the library and in this case, you dont even have to use a microcontroller

```cpp
// read the state of the pushbutton value:
  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  buttonState3 = digitalRead(buttonPin3);
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState1 == HIGH) {

    const char *msg = "#1";
    driver.send((uint8_t *)msg, strlen(msg));
    driver.waitPacketSent();

  } else {
    if (buttonState2 == HIGH) {

      const char *msg = "#2";
      driver.send((uint8_t *)msg, strlen(msg));
      driver.waitPacketSent();

    } else {
      if (buttonState3 == HIGH) {
        const char *msg = "#0";
        driver.send((uint8_t *)msg, strlen(msg));
        driver.waitPacketSent();
      } else {
        delay(1);
      }
    }
  }
```
> The states of the buttons are queried and depending on which button was pressed, either: 1. "#1" for right blinking 2. "#2" for left blinking or 3. "#0" to switch off the blinkers, is sent. At the digital inputs of the Arduino there is also a 10k ohm pull-down resistor for each button, so that the button press can be recognized correctly.
