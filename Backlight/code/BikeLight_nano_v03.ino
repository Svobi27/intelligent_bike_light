#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Adafruit_LIS3DH.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <RH_ASK.h>
#include <SPI.h> // Not actualy used but needed to compile

//driver for the RF Receiver
RH_ASK driver;
int indicatorRight = 8;
int indicatorLeft = 7;
uint8_t buf[2];
uint8_t buflen = sizeof(buf);
// I2C
Adafruit_LIS3DH lis = Adafruit_LIS3DH();

//TX and RX for the GPS
static const int TXPin = 4, RXPin = 3;
static const uint32_t GPSBaud = 9600;
// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

// Backlight Pin
const int ledPin =  12;// the number of the LED pin

int ledState = LOW;             // ledState used to set the backlight

unsigned long previousMillis = 0;        // will store last time LED was updated

long interval;          // interval at which to blink (milliseconds)
long intervalRef = 800;
void setup() {
  Serial.begin(9600);
  //start comm with GPS module
  ss.begin(GPSBaud);
  // set the digital pin as output:
  pinMode(ledPin, OUTPUT);
  
//start te accelometer
  Serial.println("LIS3DH test!");

  if (! lis.begin(0x18)) {   // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("LIS3DH found!");

   lis.setRange(LIS3DH_RANGE_2_G);   // 2, 4, 8 or 16 G!

  Serial.print("Range = "); Serial.print(2 << lis.getRange());
  Serial.println("G");
//init RF driver
  if (!driver.init())
         Serial.println("init failed");
//set the indicator LED's
  pinMode(indicatorRight, OUTPUT);
  pinMode(indicatorLeft, OUTPUT);
}

void loop() {


//get messages from the 433Mhz Trabsmitter
    if (driver.recv(buf, &buflen)) // Non-blocking
    {
      // Message with a good checksum received, dump it.
      Serial.print("Message: ");
      Serial.println(buf[1]);         
    }
  

//get the acceloration data
  while(1){
  lis.read(); 
  sensors_event_t event;
  lis.getEvent(&event);

      if(event.acceleration.x < -0.7){
      digitalWrite(ledPin, HIGH);
      delay(100);
      continue;
      }
      else{
        break;
      }
  }

  tickerLED();//sets the backlight
  
// check for GPS data
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      getSpeed();

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
    while(true);
  }

}

//process the speed in km/h
void getSpeed(){
  if (gps.speed.isValid())
  {
    Serial.print(F("KMH:  "));
    Serial.print(gps.speed.kmph());
    interval = intervalRef - (gps.speed.kmph() * 20);
    if(interval < 200){
      interval = 200;
    }
  }
  else
  {
    interval = intervalRef;
    Serial.print(F("INVALID"));
  }
  Serial.println();
}

void tickerLED(){
// check to see if it's time to blink the LED; that is, if the difference
  // between the current time and last time you blinked the LED is bigger than
  // the interval at which you want to blink the LED.
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);

    if(buf[1] == 49){
      digitalWrite(indicatorRight, ledState);
    }else{
       digitalWrite(indicatorRight, LOW);
    }
     if(buf[1] == 50){
      digitalWrite(indicatorLeft, ledState);
    } else{
      digitalWrite(indicatorLeft, LOW);
    }
    if(buf[1] == 48){
      digitalWrite(indicatorLeft, LOW);
      digitalWrite(indicatorRight, LOW);
    } 
    
  }
}
